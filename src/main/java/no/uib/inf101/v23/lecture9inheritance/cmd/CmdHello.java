package no.uib.inf101.v23.lecture9inheritance.cmd;

public class CmdHello implements Command {
  @Override
  public String run(String[] args) {
    return "Hello, world!";
  }

  @Override
  public String getName() {
    return "hello";
  }
}
