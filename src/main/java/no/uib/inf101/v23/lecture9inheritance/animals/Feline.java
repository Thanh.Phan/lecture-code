package no.uib.inf101.v23.lecture9inheritance.animals;

import java.util.Objects;

public class Feline extends Mammal {

  public Feline(String name) {
    super(name);
  }

  @Override
  public void eat(String food) {
    if (Objects.equals(food, "veggie")) {
      System.out.println(this.name + ": Yack! Felines don't eat veggies");
      return;
    }
    super.eat(food);
  }
}
