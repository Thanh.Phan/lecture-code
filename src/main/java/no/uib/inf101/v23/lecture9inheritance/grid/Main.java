package no.uib.inf101.v23.lecture9inheritance.grid;

public class Main {

  public static void main(String[] args) {
    GridDimensionRecord dim = new GridDimensionRecord(10, 20);
    System.out.println(getWidthPerCell(100, dim, 10));

    MyDoublesSet dc = new MyDoublesSet();
    double largest = findLargest(dc);
    System.out.println(largest);
  }

  public static double getWidthPerCell(double totalWidth, GridDimension dim, int margin) {
    return totalWidth - ((double) (margin * (dim.cols() + 1))) / dim.cols();
  }

  public static Double findLargest(DoubleCollection collection) {
    Double largest = null;
    for (Double d : collection.getDoubles()) {
      if (largest == null || d > largest) {
        largest = d;
      }
    }
    return largest;
  }
}
