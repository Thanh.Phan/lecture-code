package no.uib.inf101.v23.lecture9inheritance.grid;

public record GridDimensionRecord(int rows, int cols) implements GridDimension { }
