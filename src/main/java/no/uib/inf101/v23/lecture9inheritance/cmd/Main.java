package no.uib.inf101.v23.lecture9inheritance.cmd;

import no.uib.inf101.v23.lecture9inheritance.cmd.CmdHello;
import no.uib.inf101.v23.lecture9inheritance.cmd.Command;

import java.util.HashMap;
import java.util.Map;

public class Main {

  private Map<String, Command> allCommands = new HashMap<>();

  public static void main(String[] args) {
    Main main = new Main();
    main.run(args);
  }

  private void run(String[] args) {
    Command cmd = new CmdHello();
    this.allCommands.put(cmd.getName(), cmd);
    cmd = new CmdAdd();
    this.allCommands.put(cmd.getName(), cmd);

    System.out.println(this.executeCommand("hello", new String[] {}));
    System.out.println(this.executeCommand("add", new String[] {"2", "3"}));
  }

  public String executeCommand(String commandName, String[] args) {
    Command cmd = this.allCommands.get(commandName);
    if (cmd != null) {
      String result = cmd.run(args);
      return result;
    }
    // ...
    return null;
  }
}
