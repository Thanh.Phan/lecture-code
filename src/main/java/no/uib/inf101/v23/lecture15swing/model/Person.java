package no.uib.inf101.v23.lecture15swing.model;

public record Person(String name, int age) {
}
