package no.uib.inf101.v23.lecture10;

import no.uib.inf101.v23.lecture9inheritance.animals.Lion;
import no.uib.inf101.v23.lecture9inheritance.animals.Mammal;

public class Main {
    
    public static void main(String[] args) {
        shout(new Lion("Pål"));
       
    }

    public static <T extends Mammal> void shout(T elem) {
        System.out.println(elem + "!!!!");
    }
}
