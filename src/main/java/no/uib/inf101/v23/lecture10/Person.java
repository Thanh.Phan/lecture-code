package no.uib.inf101.v23.lecture10;

import java.util.Objects;

public class Person extends Object {

    String name;
    String id;
  
    public Person(String name, String id) {
        this.name = name;
        this.id = id;
    }

    public String getName() {
        return this.name;
    }


    public static void main(String[] args) {
        Person p1 = new Person("Ola", "111111 12345");
        Person p2 = new Person("Ola", "111111 12345");

        p1.getName();
        p1.equals(p2);
    }

}