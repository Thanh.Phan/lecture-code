package no.uib.inf101.v23.lecture.javacourse.programFlow;

import java.util.Scanner;


/**
 * Write a method that asks the user for integers. 
 * If the user enters 0 it should stop asking for input and print the sum of the numbers
 */
public class LoopExampleTask {

    static Scanner sc = new Scanner(System.in);    

    public static void main(String[] args) {
        System.out.println(readIntegersFromUser());
    }

    public static int readIntegersFromUser() {
        int sum = 0;

        int userInteger = -1;
        while (userInteger != 0) {
            String userString = readInput("Please enter integer:");
            userInteger = Integer.parseInt(userString);
            sum += userInteger;
        }
        return sum;
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public static String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.nextLine();
        return userInput;
    }

}
