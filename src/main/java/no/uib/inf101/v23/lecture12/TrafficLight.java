package no.uib.inf101.v23.lecture12;

public class TrafficLight {

	//field variables
	private boolean red; 
	private boolean yellow; 
	private boolean green;
	
	public TrafficLight(boolean red, boolean yellow, boolean green) {
		if(red && green)
			throw new IllegalArgumentException("Can not construct trafficLight with both red and green light on");
		if(!red && !green && !yellow)
			throw new IllegalArgumentException("Can not construct trafficLight with all lights off");
		if(yellow && green)
			throw new IllegalArgumentException("Can not construct trafficLight with both yellow and green light on");
		this.red= red;
		this.green = green;
		this.yellow = yellow;
	}

	public TrafficLight() {
		this(getDefaultValue1(),false,false);
	}

	//ikke lurt
//	public void setGreen(boolean green) {
//		this.green = green;
//	}
	
	private static boolean getDefaultValue1() {
		return true;
	}

	public boolean redIsOn() {
		return red;
	}

	public boolean greenIsOn() {
		return green;
	}

	public boolean yellowIsOn() {
		return yellow;
	}
	
	void next() {
		if(redIsOn()) {
			if(!yellowIsOn()) {
				yellow = true;
			}else {
				red = false;
				yellow=false;
				green=true;
			}
		}else {
			if(greenIsOn()) {
				green=false;
				yellow=true;
			}else {
				yellow=false;
				red=true;
			}
		}
	}
}
