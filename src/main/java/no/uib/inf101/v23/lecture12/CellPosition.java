package no.uib.inf101.v23.lecture12;

/**
 * This class represents a position on a grid.
 * That means indices for row and column.
 * <p>
 * CellPosition is Immutable, this means the only way to make a new
 * CellPosition is to call the constructor.
 */
public record CellPosition(int row, int col) implements Comparable<CellPosition> {

	/**
	 * This method is just for convenience.
	 * 
	 * @see GridDirection#getNeighbor(CellPosition)
	 * @param dir direction in which to get neighbouring CellPosition
	 * @return a neighbour CellPosition
	 */
	public CellPosition getNeighbor(GridDirection dir) {
		return dir.getNeighbor(this);
	}

	@Override
	public int compareTo(CellPosition o) {
		int sum = row+col;
		int sumo = o.row+o.col;
		if(sum<sumo)
			return -1;
		if(sum>sumo)
			return 1;
		
		return Integer.compare(row, o.row);
	}
	
	@Override
	public String toString(){
		return "("+row+","+col+")";
	}
}
