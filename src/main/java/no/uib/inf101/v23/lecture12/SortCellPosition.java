package no.uib.inf101.v23.lecture12;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class SortCellPosition {

	public static void main(String[] args) {
		List<CellPosition> list = generatePositions(10);
		System.out.println(list);
		Collections.sort(list);
		System.out.println(list);
		RowwiseComparator rc = new RowwiseComparator();
		Collections.sort(list, rc);
		System.out.println(list);
	}

	static List<CellPosition> generatePositions(int n){
		List<CellPosition> list = new ArrayList<>();
		Random rand = new Random();
		for(int i=0; i<n; i++) {
			list.add(new CellPosition(rand.nextInt(10), rand.nextInt(10)));
		}
		return list;
	}
}
