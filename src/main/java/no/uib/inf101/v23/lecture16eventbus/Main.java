package no.uib.inf101.v23.lecture16eventbus;

import no.uib.inf101.v23.lecture16eventbus.eventbus.EventBus;
import no.uib.inf101.v23.lecture16eventbus.model.PersonList;
import no.uib.inf101.v23.lecture16eventbus.view.ViewMain;

import javax.swing.JFrame;

public class Main {

  public static void main(String[] args) {
    EventBus eventBus = new EventBus();
    PersonList model = new PersonList();
    ViewMain view = new ViewMain(model, eventBus);

    JFrame frame = new JFrame("INF101 Person List");
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.setContentPane(view.getMainPanel());
    frame.pack();
    frame.setVisible(true);
  }
}
