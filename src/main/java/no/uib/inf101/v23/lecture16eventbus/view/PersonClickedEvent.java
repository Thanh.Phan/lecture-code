package no.uib.inf101.v23.lecture16eventbus.view;

import no.uib.inf101.v23.lecture16eventbus.eventbus.Event;
import no.uib.inf101.v23.lecture16eventbus.model.Person;

public record PersonClickedEvent(Person personClicked) implements Event {}
