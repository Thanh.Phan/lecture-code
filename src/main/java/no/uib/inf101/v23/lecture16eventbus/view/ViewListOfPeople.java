package no.uib.inf101.v23.lecture16eventbus.view;


import no.uib.inf101.v23.lecture16eventbus.eventbus.EventBus;
import no.uib.inf101.v23.lecture16eventbus.model.Person;
import no.uib.inf101.v23.lecture16eventbus.model.PersonList;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.DefaultListModel;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JScrollPane;


public class ViewListOfPeople {

  //////////////////////////////////////////////
  ////            TEST CODE                 ////
  //// vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv ////
  public static void main(String[] args) {
    PersonList model = new PersonList();
    for (int i = 0; i < 10; i++) {
      model.addPerson(new Person("Adam", i));
      model.addPerson(new Person("Eva", i));
    }
    
    ViewListOfPeople view = new ViewListOfPeople(model, new EventBus());
    
    JFrame frame = new JFrame("MOCK");
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.setContentPane(view.mainPanel);
    frame.pack();
    frame.setVisible(true);
  }
  //// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ ////
  ////           /TEST CODE                 ////
  //////////////////////////////////////////////
  
  
  private PersonList model;
  private JScrollPane mainPanel;
  private EventBus eventBus;
  
  public ViewListOfPeople(PersonList model, EventBus eventBus) {
    this.eventBus = eventBus;
    this.model = model;
    
    // Make a list of Persons and add them to the mainPanel
    DefaultListModel<Person> listModel = new DefaultListModel<>();
    JList<Person> list = new JList<>(listModel);
    for (Person person : model.getPersons()) {
      listModel.addElement(person);
    }

    list.addMouseListener(new MouseAdapter() {
      @Override
      public void mouseClicked(MouseEvent event) {
        if (event.getClickCount() == 2) {
          Person person = list.getSelectedValue();
          eventBus.post(new PersonClickedEvent(person));
        }
      }
    });
    
    this.mainPanel = new JScrollPane(list);
  }
  
  public JComponent getMainPanel() {
    return this.mainPanel;
  }
}
