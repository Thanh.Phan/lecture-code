package no.uib.inf101.v23.lecture11;

import static no.uib.inf101.v23.lecture11.Range.*;

public class UseRange {

	public static void main(String[] args) {
		RangeIterator ri = new RangeIterator(1, 10);
		while(ri.hasNext()) {
			System.out.print(ri.next()+" ");
		}
		System.out.println();
		Range range = new Range(1,10);
		for(int i: range) {
			System.out.print(i+" ");
		}
		System.out.println();
		for(int i:range(1, 10)) {
			System.out.print(i+" ");
		}

	}

}
