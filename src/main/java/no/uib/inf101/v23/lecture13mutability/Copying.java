package no.uib.inf101.v23.lecture13mutability;

import java.util.Arrays;

public class Copying {
  public static void main(String[] args) {
    copyTetris();
  }

  private static void copyTetris() {

    Boolean[][] yourTetrisPiece = new Boolean[][] {
        {  true,  true,  true },
        { false,  true, false }
    };

    // Dyp kopiering
    Boolean[][] theirTetrisPiece = makeDeepCopy(yourTetrisPiece);

    System.out.println(Arrays.deepToString(theirTetrisPiece));
    yourTetrisPiece[0][0] = false;
    System.out.println(Arrays.deepToString(theirTetrisPiece));

  }

  private static <T> T[] makeDeepCopy(T[] org) {
    T[] result = getNewArrayInSameClassAs(org, org.length);

    for (int i = 0; i < org.length; i++) {
      T item = org[i];
      if (item instanceof Object[]) {
        Object[] item_array = (Object[]) item;
        @SuppressWarnings("unchecked") // danger zone
        T copy = (T) makeDeepCopy(item_array);
        result[i] = copy;
      }
      else {
        result[i] = item;
      }
    }
    return result;
  }

  /**
   * Returns a new array of the same class as the given array, but with the given length.
   * The array is initialized with null values.
   * @param org The array to get the class from
   * @param length  The length of the new array
   * @return  A new array of the same class as the given array, but with the given length.
   * @param <T> The type of the array
   */
  private static <T> T[] getNewArrayInSameClassAs(T[] org, int length) {
    // Koden i denne metoden er utenfor pensum i INF101
    //noinspection unchecked
    return (T[]) java.lang.reflect.Array.newInstance(org.getClass().getComponentType(), length);
  }
}
