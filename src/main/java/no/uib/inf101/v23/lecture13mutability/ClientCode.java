package no.uib.inf101.v23.lecture13mutability;

public class ClientCode {
  public static void main(String[] args) {
    MyImmutable mi = new MyImmutable(5, 9);
    // uansett hva jeg gjør her i klientkoden, så vil ikke
    // verdiene i objekter i MyImmutable-klassen bli endret:

    // mi.myvals[0] = 10; // ikke lov, siden myvals er private
    // mi.myvals = new int[] { 10, 20 }; // ikke lov, myvals er private
    // mi.setA(10); // noen slik metode finnes ikke
  }
}
