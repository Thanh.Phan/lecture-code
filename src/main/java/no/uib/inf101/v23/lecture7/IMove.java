package no.uib.inf101.v23.lecture7;

public interface IMove {

	/**
	 * This method computes how much damage is dealt by attacker to defender
	 * @param attacker
	 * @param defender
	 * @return
	 */
	int computeDamage(IPokemon attacker, IPokemon defender);
}
