package no.uib.inf101.v23.lecture7;

public class StrengthMove implements IMove{

	@Override
	public int computeDamage(IPokemon attacker, IPokemon defender) {
		return attacker.getStrength()-defender.getStrength();
	}

}
